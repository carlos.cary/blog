package com.ucbcba.blog;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

/**
 * Created by amolina on 19/09/17.
 */
@SpringBootApplication
/*
@ComponentScan({"com.blog.request"})
@EntityScan("com.blog.domain")
@EnableJpaRepositories("com.blog.repository")*/
public class BlogApplication {

    public static void main(String[] args) {
        SpringApplication.run(BlogApplication.class, args);
    }

}
