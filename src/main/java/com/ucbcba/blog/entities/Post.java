package com.ucbcba.blog.entities;

import javax.persistence.*;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.math.BigDecimal;
import java.sql.Date;
import java.util.Set;

/**
 * Created by amolina on 19/09/17.
 */
@Entity
public class Post {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;

    @NotNull
    @Size(min = 1, max = 20,message = "Debe ser mayor a 1 y menor a 20 caracteres")
    private String title;

    @NotNull
    @Size(min = 1, max = 100,message = "Debe ser mayor a 1 y menor a 100 caracteres")
    private String text;

    ///OBSERVACION
    @OneToMany(mappedBy = "post")
    private Set<Comment> comments;



    @ManyToOne
    @JoinColumn(name="user_id")
    private User user;

    @NotNull
    @Column(columnDefinition="int(5) default '0'")  ///Q ES ESTO?
    private Integer likes = 0 ;


    private java.sql.Date date;

    @ManyToOne
    @JoinColumn(name="category_id")
    private Category category;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }


    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Integer getLikes() {
        return likes;
    }

    public void setLikes(Integer likes) {
        this.likes = likes;
    }

    public Set<Comment> getComments() {
        return comments;
    }

    public void setComments(Set<Comment> comments) {
        this.comments = comments;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }
}
