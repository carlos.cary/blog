package com.ucbcba.blog.entities;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.sql.Date;
import java.util.Calendar;

/**
 * Created by amolina on 26/09/17.
 */
@Entity
public class Comment {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;


    @NotNull
    @Size(min = 1, max = 50,message = "Debe ser mayor a 1 y menor 50")
    private String text;

    @NotNull
    @ManyToOne
    @JoinColumn(name="post_id")
    private Post post;

    public Integer getLikes() {
        return likes;
    }

    public void setLikes(Integer likes) {
        this.likes = likes;
    }

    @NotNull
    @Column(columnDefinition="int(5) default '0'")  ///Q ES ESTO?
    private Integer likes = 0 ;


    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    private java.sql.Date date=new Date(Calendar.getInstance().getTime().getTime());

    public Comment(){

    }
    public Comment(Post post){
        this.post = post;

    }


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Post getPost() {
        return post;
    }

    public void setPost(Post post) {
        this.post = post;
    }
}
