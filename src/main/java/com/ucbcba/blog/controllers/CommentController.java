package com.ucbcba.blog.controllers;

import com.ucbcba.blog.entities.Comment;
import com.ucbcba.blog.entities.Post;
import com.ucbcba.blog.services.CommentService;
import com.ucbcba.blog.services.PostService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.validation.Valid;

/**
 * Created by amolina on 26/09/17.
 */
@Controller
public class CommentController {

    private CommentService commentService;

    @Autowired
    public void setCommentService(CommentService commentService) {
        this.commentService = commentService;
    }




    @RequestMapping(value = "/comment", method = RequestMethod.POST)
    public String save(@Valid Comment comment, BindingResult bindingResult, Model model) {
        if(bindingResult.hasErrors()){
            model.addAttribute("comments", commentService.listAllComments());
            return "redirect:/post/"+comment.getPost().getId();
        }
        commentService.saveComment(comment);
        return "redirect:/post/"+comment.getPost().getId();
    }
    
    @RequestMapping(value = "/comment/eliminar/{id}", method = RequestMethod.GET)
    public String deleteComment(@PathVariable Integer id, Model model) {
        Comment comment = commentService.getCommentById(id);
        commentService.deleteComment(id);
        return "redirect:/post/"+comment.getPost().getId();
    }

    @RequestMapping(value = "/comment/like/{id}", method = RequestMethod.GET)
    public String like(@PathVariable Integer id, Model model){
        Comment comment = commentService.getCommentById(id);
        comment.setLikes(comment.getLikes() + 1);
        commentService.saveComment(comment);
        return "redirect:/post/"+comment.getPost().getId();
    }


    @RequestMapping(value = "/comment/dislike/{id}", method = RequestMethod.GET)
    public String dislike(@PathVariable Integer id, Model model) {
        Comment comment = commentService.getCommentById(id);
        if (comment.getLikes() > 0){
            comment.setLikes(comment.getLikes() - 1);
            commentService.saveComment(comment);
        }
        return "redirect:/post/"+comment.getPost().getId();
    }
}
