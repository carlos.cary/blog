package com.ucbcba.blog.controllers;

import com.ucbcba.blog.entities.User;
import com.ucbcba.blog.services.CommentService;
import com.ucbcba.blog.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
@Controller
public class UserController {

    private UserService userService;

    @Autowired
    public void setUserService(UserService userService){
        this.userService = userService;
    }
    @RequestMapping(value = "/user", method = RequestMethod.POST)
    public String save(User user, Model model){
        userService.saveUser(user);
        return "redirect:/users";
    }

    @RequestMapping(value = "/user/new", method = RequestMethod.GET)
    public String newPost(Model model) {
        model.addAttribute("user", new User());
        model.addAttribute("categories", userService.listAllUsers());
        return "userForm";
    }
    @RequestMapping(value = "/user/eliminar/{id}", method = RequestMethod.GET)
    public String deleteUser(@PathVariable Integer id, Model model){
        userService.deleteUser(id);
        return "redirect:/users";
    }

    @RequestMapping(value = "/user/editar/{id}", method = RequestMethod.GET)
    public String editUser(@PathVariable Integer id, Model model){
        model.addAttribute("user", userService.getUserById(id));
        return "redirect:/users";
    }
    @RequestMapping(value = "/users", method = RequestMethod.GET)
    public String list(Model model) {
        model.addAttribute("users", userService.listAllUsers());
        return "users";
    }

    @RequestMapping(value = "/user", method = RequestMethod.GET)
    public String showUser(@PathVariable Integer id, Model model){
        User user = userService.getUserById(id);
        model.addAttribute("user", user);
        return "user";
    }

}
