insert into category(name) values("Deportes");
insert into category(name) values("Cultura");

insert into user(name) values("Carlitoj");
insert into user(name) values("Howard");

insert into post(date,likes,text,title,category_id,user_id) values("2017-10-12",0,"David Marchante","Powerlifting",1,1);
insert into post(date,likes,text,title,category_id,user_id) values("2017-10-12",0,"Albert E.","Fisica",2,2);

insert into comment(text,post_id,likes) values("Es muy fuerte!",1,0);
insert into comment(text,post_id,likes) values("El mejor del mundo!",1,0);
insert into comment(text,post_id,likes) values("Es muy listo!",2,0);
insert into comment(text,post_id,likes) values("Revoluciono al mundo!",2,0);